import sys
import json
import uuid
import random
import flask
import flask_socketio

app = flask.Flask(__name__)
app.config["STARTING_CHIPS"] = 25
app.config["STARTING_PLAYERS"] = 5
player_list = []
game = None
response = ""
loaded = 0
socket = flask_socketio.SocketIO(app)

class poker:
    class Card:
        def __init__(self, suit, value):
            self.suit = suit
            self.value = value
            self.color = ["#333", "#f00", "#f00", "#333"][suit]
            if value > 10:
                self.htmle = "&#" + str(127138 + 16 * suit + value) + ";"
            else:
                self.htmle = "&#" + str(127137 + 16 * suit + value) + ";"
            self.html = "<span style='color: %s;'>%s</span>" % (self.color, self.htmle)
    class Deck:
        def __init__(self):
            self.cards = []
            for s in range(4):
                for v in range(13):
                    self.cards.append(poker.Card(s, v))
        def deal(self, num):
            random.shuffle(self.cards)
            return [self.cards.pop() for i in range(num)]
    class Player:
        def __init__(self, username):
            global player_list
            self.username = username
            self.uuid = str(uuid.uuid4())
            self.hand = []
            self.game = None
            self.chips = app.config['STARTING_CHIPS']
            self.player_idx = len(player_list)
            self.betting_state = "u"
        def __str__(self):
            return "{0.username} {0.uuid}".format(self)
        def bet(self, amount):
            self.chips -= amount
            self.game.pot += amount
    class Game:
        def __init__(self, player_list):
            self.deck = poker.Deck()
            self.player_list = player_list
            self.players = len(self.player_list)
            self.pot = 0
            for p in self.player_list:
                p.hand = self.deck.deal(2)
                p.game = self
            self.flop = self.deck.deal(3)
            self.turn = self.deck.deal(1)[0]
            self.river = self.deck.deal(1)[0]
            self.stage = 0 # 0:Pre-Flop 1:Flop 2:Turn 3:River
            self.current_bet = 0
            self.total_bet = 0
            self.player_turn = 0
        def not_me(self, player_idx):
            return [x for i,x in enumerate(self.player_list) if i != player_idx]


@app.route("/")
def start():
    return flask.render_template("start.html")

@app.route("/create_player")
def create_player():
    global player_list
    player = poker.Player(flask.request.args.get("name"))
    player_list.append(player)
    socket.emit('player joined')
    print " * Player Added: " + str(player)
    return flask.render_template("waiting_room.html",
        player_idx = player.player_idx,
        required_players = app.config["STARTING_PLAYERS"],
        redirect_url = flask.url_for(".play") + "?player_idx=" + str(player.player_idx))

@socket.on('enough players')
def enough_players():
    global game
    global player_list
    game = poker.Game(player_list)
    print " * Game Created"
    socket.emit("players created")

@app.route("/play")
def play():
    global game
    global loaded
    loaded += 1
    socket.emit('player loaded')
    return flask.render_template("play.html", game = game,
        player_idx = int(flask.request.args.get("player_idx")))

@socket.on('all players loaded')
def all_players_loaded():
    socket.emit('bet', {"player_idx": player_turn})

@socket.on('done betting')
def done_betting(json):
    global game
    player = game.player_list[json['player_idx']]
    print str(player) + " just bet " + json['response']
    move = json['response'][0]
    amount = int(json['response'][1:])
    if move == "f":
        player.betting_state = "f"
    if move == "c":
        player.betting_state = "c"
        player.bet(game.current_bet)
    if move == "r":
        for p in game.player_list:
            p.betting_state = "u"
        player.betting_state = "c"
        game.current_bet = amount
        game.total_bet += amount
        game.player_turn = 0

    search = None
    for p in game.player_list:
        if p.betting_state == "u":
            search = p.player_idx
            break
    if search != None:
        game.player_turn = search
        socket.emit('bet', {"player_idx": game.player_turn})
    else:
        socket.emit('done stage betting')

@socket.on('advance stage')
def advance_stage():
    global game
    if game.stage == 3:
        socket.emit('reveal stage', {'stage': game.stage})
        sys.stop(0)
    else:
        socket.emit('reveal stage', {'stage': game.stage})
        game.stage += 1
        game.player_turn = 0
        socket.emit('bet', {"player_idx": game.player_turn})


socket.run(app, host = "0.0.0.0", debug = True, port = 5000)
